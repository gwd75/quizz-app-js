const questions = [
    {
        question : 'Quel est le seul joueur à avoir gagné 3 coupes du mondes ?',
        answers : [
            { text : 'Lionel Messi' , correct: false},
            { text : 'Diego Maradona' , correct: false},
            { text : 'Pelé' , correct: true},
            { text : 'Johan Cruyff' , correct: false},
        ]
    },
    {
        question : "Qui a gagné le ballon d'or en 2012 ?",
        answers : [
            { text : 'Andres Iniesta' , correct: false},
            { text : 'Lionel Messi' , correct: true},
            { text : 'Cristiano Ronaldo' , correct: false},
            { text : 'Xavi' , correct: false},
        ]
    },
    {
        question : "Quelle équipe a gagné la coupe du monde 1998 ?",
        answers : [
            { text : "L'Allemagne" , correct: false},
            { text : 'Le Brésil' , correct: false},
            { text : "L'Argentine" , correct: false},
            { text : 'La France' , correct: true},
        ]
    },
    {
        question : "Quel est le montant du transfert de Neymar au PSG à l'été 2017 ?",
        answers : [
            { text : "1M€" , correct: false},
            { text : '500M€' , correct: false},
            { text : "222M€" , correct: true},
            { text : '243M€' , correct: false},
        ]
    },
    {
        question : "Combien de ligues des champions a gagné le Real Madrid ?",
        answers : [
            { text : "3" , correct: false},
            { text : '11' , correct: false},
            { text : "13" , correct: true},
            { text : '12' , correct: false},
        ]
    },
    {
        question : "Combien de fois l'OM a gagné la Ligue 1 ?",
        answers : [
            { text : "10 fois" , correct: false},
            { text : '8 fois' , correct: false},
            { text : "11 fois" , correct: false},
            { text : '9 fois' , correct: true},
        ]
    },
    {
        question : "Dans combien de club Lionel Messi a-t-il évolué dans sa carrière professionelle ?",
        answers : [
            { text : "3 clubs" , correct: false},
            { text : '2 clubs' , correct: true},
            { text : "1 club" , correct: false},
            { text : '5 clubs' , correct: false},
        ]
    },
    {
        question : "Quel équipe a gagné la Ligue 1 en 2009-2010 ?",
        answers : [
            { text : "Bordeaux" , correct: false},
            { text : 'Lyon' , correct: false},
            { text : "Marseille" , correct: true},
            { text : 'Paris' , correct: false},
            { text : 'Monaco' , correct: false},
            { text : 'Lille' , correct: false},
        ]
    },
    {
        question : "Quel joueur allemand à marqué 16 buts en phase finale de coupe du monde ?",
        answers : [
            { text : "Robert Lewandowski" , correct: false},
            { text : 'Arjen Robben' , correct: false},
            { text : "Miroslav Klose" , correct: true},
            { text : 'Manuel Neuer' , correct: false},
        ]
    },
    {
        question : "Quel joueur de football représente la plus grosse vente de l'histoire de Lille ?",
        answers : [
            { text : "Eden Hazard" , correct: false},
            { text : 'Victor Osimhen' , correct: true},
            { text : "Mike Maignan" , correct: false},
            { text : 'Dimitri Payet' , correct: false},
        ]
    },
    {
        question : "Combien de buts à marqué Lionel Messi sur l'année 2012 ?",
        answers : [
            { text : "92" , correct: false},
            { text : '90' , correct: false},
            { text : "93" , correct: false},
            { text : '91' , correct: true},
        ]
    },
]